const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UsersSchema = new Schema({
    _id: Number,
    name: String,
    age: Number,
    address: String,
    team: String
});

const User = mongoose.model('User', UsersSchema);

module.exports = User;
