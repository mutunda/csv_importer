// Required package
const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
const async = require('async');
const eachSeries = require('async/eachSeries');
const each = require('async/each');
const fs = require('fs');
const path = require('path');
const multer = require('multer');
const csv = require('fast-csv');

//Import models
const User = require('./model/userModel');


//DB connection
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://fottym:wSSNhz2QGDnr2NHF4wWkPxGSaj9wBb@ds115712.mlab.com:15712/testnode');
mongoose.connection.once('open', function () {
    console.log('Connected to DB')
}).on('error', function (error) {
    console.log(error);
});


//Config upload destination
const upload = multer({dest: 'uploads/'});

// Configure server
const app = express();
app.use(cors());
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
const port = 4321 || process.env.PORT;
// const port = 4321;

const router = express.Router();

//Match routes
router.get('/', function (req, res) {
    res.json({name: "csv importer "})
}, function (error) {
    console.log(error)
});

router.post('/import', upload.single('csvFile'), function (req, res) {
    const csvFile = req.file;
    const columns = ["_id", "name", "age", "address", "team"];
    const data = fs.createReadStream(path.join(__dirname + '/uploads', csvFile.filename), {encoding: 'utf8'});

    let stream = data;
    let convertedCSV = [];
    csv.fromStream(stream, {headers: columns}).on("data", function (data) {
        convertedCSV.push(data);


    }).on("end", function () {
        User.collection.createIndex({"$**": "text"});

        let startTime = Date.now();
        let count = 0;
        let c = Math.round(convertedCSV.length / 990);
        let fakeArr = [];
        fakeArr.length = c;

        let docsSaved = 0;
        async.each(fakeArr, function (item, callback) {

            let sliced = convertedCSV.slice(count, count + 999);
            sliced.length;
            count = count + 999;
            if (sliced.length !== 0) {
                User.collection.insert(sliced, function (err, docs) {
                    docsSaved += docs.ops.length;
                    callback(null, 'Done');
                })
            } else {
                callback(null,'Everything is Okay');
            }
        }, function (err) {
            console.log(" Importer JS > BULK INSERT AMOUNT: ", convertedCSV.length, "docsSaved  ", docsSaved, " DIFF TIME:", Date.now() - startTime);
            res.json({message:'Inserted ' + convertedCSV.length , saved: docsSaved, time: Date.now() - startTime , error: err})
        });


    });


});

router.post('/search', function (req, res) {
    let q = req.body.query;

    User.find({$text: {$search: q}})
        .limit(20)
        .then((data) => {
                console.log(data);
                res.json(data);
            }
        ).catch((err) => {
        console.log(err)
    })


});


// Set default URL
app.use('/api', router);

//Run server
app.listen(port, function () {
    console.log('the server running on http://localhost:' + port);
});



