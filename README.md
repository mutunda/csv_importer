#Import CSV and Find By.

#Frameworks
The App is build on React as frontend and Express and Mongo DB in the backend(basically MERN stack)

##How to run the application
I have Installed all the dependencies with `yarn` but it works with `npm` as well.
1. run `npm install` to install all the dependencies 
2. run `npm start` to fire up both back and frontend at once.
2. from the browser go to `http://localhost:3456/` to open the frontend.
3. the backend runs on `http://localhost:4321` if you just want to use the api.
