import React, {Component} from 'react';
import {Alert, Button} from 'react-bootstrap'


export default class AlertComponent extends Component {
    constructor(props) {
        super();
        this.handleAlertDismiss = this.handleAlertDismiss.bind(this);
        this.state = {
            alertVisible: props.loading
        }

    }

    handleAlertDismiss() {
        this.setState({alertVisible: false});
    }

    render() {
        let details = this.props.response;
        console.log(this.state.alertVisible);
        if(this.state.alertVisible){
            return (
                <Alert bsStyle="success" onDismiss={this.handleAlertDismiss}>
                    <h4>Success</h4>
                    <p> {details.message}</p>
                    <p> {details.saved}</p>
                    <p> {details.time} ms</p>
                </Alert>
            );
        }
        return(<div></div>)

    }

}



