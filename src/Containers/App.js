import React, {Component} from 'react';
import '../Styles/App.css';
import {Link} from 'react-router'


const App = (props) => {
    return (
        <div className="container">
            <div className="jumbotron">
                <h1>Import a Huge CSV or Look for Imported Users</h1>
                <Link className="btn btn-lg btn-primary" to="search">Search For Users</Link>
                <Link className="btn btn-lg btn-success" to="import">Import CSV</Link>
            </div>
            {props.children}
        </div>
    );
};

export default App;
