import React, {Component} from 'react';
import {SearchResult} from '../Components/SearchResult'
import axios from 'axios';


export class Search extends Component {
    constructor(props) {
        super();
        this.state = {
            users: [],
            loading: false,
        };
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        this.setState({loading: true});
        let query = {query: event.target[0].value};
        let users = await axios.post('http://localhost:4321/api/search', query)
            .then((res) => {
                    this.setState({loading: false});
                    return res.data;
                }
            )
            .catch(err => alert(err));
        this.setState({users});


    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <form onSubmit={this.handleSubmit} onKeyUp={this.suggestText} className="form-inline">
                            <div className="form-group">
                                <input type="text" id="searchField" className="form-control"/>
                            </div>
                            <input type="submit" name="query" value='Search'
                                   placeholder="search by name team color or address... " id="searchButton"
                                   className="btn btn-success"/>
                        </form>
                    </div>
                </div>
                <hr/>
                <div className="row">
                    {this.state.loading ? <p> <i className="fa fa-spinner center fa-3x" aria-hidden="true"></i>Looking... </p> : null}
                    <div className="col-md-12">
                        <SearchResult users={this.state.users}/>
                    </div>
                </div>
            </div>
        )
    }
}
