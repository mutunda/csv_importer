import React, {Component} from 'react';
import AlertComponent from './Alert'
import {Alert} from 'react-bootstrap'
import axios from 'axios';

export class Upload extends Component {
    constructor(props) {
        super();

        this.state = {
            loading: false,
            response: {}
        };

        this.handleSubmit = this.handleSubmit.bind(this);
    }


    handleSubmit(event) {
        event.preventDefault();
        this.setState({
            loading: true
        });

        if (event.target[0].files.length !== 0) {
            let csvFile = event.target[0].files[0];
            var formData = new FormData();
            formData.append('csvFile', csvFile);
            axios.post('http://localhost:4321/api/import', formData)
                .then((res) => {
                    this.setState({
                        loading: false ,
                        response: res.data
                    });
                    return res.data;
                })
                .catch(err => {
                    this.setState({
                        loading: false
                    });
                    alert(err);
                });

        } else {
            console.log(event.target);
            this.setState({
                loading: false
            });
            alert('Please select a file');
        }

    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <form onSubmit={this.handleSubmit} encType="multipart/form-data" className="form-inline">
                            <div className="form-group">
                                <input type="file" id="uploadField" accept=".csv" className="form-control"/>
                            </div>
                            <input type="submit" name="upload" value='Upload' id="uploadButton"
                                   className="btn btn-info"/>
                            <input type="reset" value='Reset' className="btn btn-danger"/>
                        </form>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-offset-2 col-md-8">
                        {this.state.loading ? <h2><span><i className="fa fa-spinner center fa-5x" aria-hidden="true"></i> </span></h2>: null}
                        {this.state.loading ? null : <AlertComponent response={this.state.response} loading={this.state.loading}/>}
                    </div>
                </div>
            </div>
        )

    }
}
