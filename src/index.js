import React from 'react';
import ReactDOM from 'react-dom';
import {Router, Route, IndexRoute, browserHistory} from 'react-router'
import createHashHistory from 'history/createHashHistory'
import App from './Containers/App';
import {Search} from './Containers/Search'
import {Upload} from './Containers/Upload'
import registerServiceWorker from './registerServiceWorker';
import './Styles/index.css';

ReactDOM.render(
    <Router history={browserHistory}>
        <Route path="/" component={App}>
            <Route path="search" component={Search}/>
            <Route path="import" component={Upload}/>
        </Route>
    </Router>, document.getElementById('root'));
registerServiceWorker();
