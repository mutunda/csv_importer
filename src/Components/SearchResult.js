import React, {Component} from 'react';



export const SearchResult = (props) => {

        return (
            <ul>
                { props.users.map( (user, i) => <li key={i} id={"result-"+i}>{user.name} {user.team } {user.age} {user.address}</li>)}
            </ul>
        )
};
